# es2csv

### 1.安装pip

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

### 2.安装git

apt-get install git

### 3.安装es2csv工具

pip install git+https://gaoyanshou@bitbucket.org/geneseeq_china/es2csv.git

### 4.导出

es2csv -u 172.16.18.24:9200 -f orderNo orderMold -r -i sample_order_report -q {} -o database.csv

指定表头导出文件
python es2csv_cli.py -u 172.16.18.24:9200 -f mrId,mrNo,patientSexValue,mainClassValue,subClassOneValue,subClassTwoValue,orderNo  -r -i mini_program_all_info -q {} -o database.csv -M 病历id,病历号,性别,癌症大类,癌症子类一,癌症子类二,订单号 