FROM registry.cn-shanghai.aliyuncs.com/geneseeq-basic/geneseeq-java-for-mysql:8
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python get-pip.py
RUN apt-get update && apt-get install -y git && pip install git+https://gaoyanshou@bitbucket.org/geneseeq_china/es2csv.git && apt-get install -y python-mysqldb
RUN pip install elasticsearch==6.3.1