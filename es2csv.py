import os
import time
import json
import codecs
import elasticsearch
import progressbar
from backports import csv
from functools import wraps
import hashlib
import time 
from pandas.io.json import json_normalize
from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool
import sys
import datetime
import dateutil.parser
import pytz

FLUSH_BUFFER = 1000  # Chunk of docs to flush in temp file
CONNECTION_TIMEOUT = 120
TIMES_TO_TRY = 3
RETRY_DELAY = 60
META_FIELDS = [u'_id', u'_index', u'_score', u'_type']

class MysqlHelpPlus(object):
    __instance = None
    __first_init = None
    def __new__(cls, db_info):
        if not cls.__instance:
            MysqlHelpPlus.__instance = object.__new__(cls)
        return cls.__instance
    
    def __init__(self, mysql_url):
        self.engine = create_engine(mysql_url,
                                echo=False,
                                poolclass=QueuePool,
                                pool_size=10,
                                pool_recycle=5,
                                pool_timeout=30, 
                                pool_pre_ping=True, 
                                max_overflow=0)
        # self.cursor, self.conn = self.db.get_conn()
        MysqlHelpPlus.__first_init = True

    def excute_sql_to_mysql(self, sql_content):
        try:
            with self.engine.connect() as conn, conn.begin():
                result = conn.execute(sql_content)
                return result
        except Exception as e:
            print e
            return None
      

# Retry decorator for functions with exceptions
def retry(ExceptionToCheck, tries=TIMES_TO_TRY, delay=RETRY_DELAY):
    def deco_retry(f):
        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries = tries
            while mtries > 0:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    print(e)
                    print('Retrying in {} seconds ...'.format(delay))
                    time.sleep(delay)
                    mtries -= 1
                else:
                    print('Done.')
            try:
                return f(*args, **kwargs)
            except ExceptionToCheck as e:
                print('Fatal Error: {}'.format(e))
                exit(1)

        return f_retry

    return deco_retry


class Es2csv:

    def __init__(self, opts):
        self.opts = opts
        self.mysql_url = self.opts.mysql_url
        self.union_uuid = self.opts.union_uuid
        self.mysql_client = MysqlHelpPlus(self.mysql_url)
        self.opts.query = self.get_es_body(self.union_uuid)
        self.uuid = ""
        self.csv_is_header = False
        self.num_results = 0
        self.scroll_ids = []
        self.scroll_time = '30m'
        self.opts.fields = self.opts.fields.split(",")
        self.opts.map_fields = self.opts.map_fields.split(",")
        self.level = self.opts.level
        self.uuid_list = []
        self.default_key_list = ["midSampleInfoTSampleCode", 
                                "midSepDnaExtractionTSampleIdLims", 
                                "midSepLibraryConstructionTSampleIdLims",
                                "midSepPoolingTSampleIdLims", 
                                "midSepSequencingTSampleIdLims", 
                                "midMedicalRecordTMrNo",
                                "midSampleOrderTOrderCode",
                                "midGeneMutationTMutId",
                                "midSepBioinformaticsQcTSampleIdLims"]
        #self.map_scv_header = self.opts.map_fields
        if  '_all' not in self.opts.map_fields:
            self.opts.map_fields = [unicode(field, "utf-8") for field in self.opts.map_fields]
            self.map_scv_header = dict(zip(self.opts.fields, self.opts.map_fields))
        #self.map_scv_header = [unicode(field, "utf-8") for field in self.opts.map_fields]
        self.csv_headers = list(META_FIELDS) if self.opts.meta_fields else []
        self.date_swtich_list = ["midSampleOrderTFirstSendData",
                                "midSampleOrderTOrderCreationDate",
                                "midMedicalRecordTBirthDate",
                                "midMedicalRecordTCreationDate",
                                "midSampleInfoTReceiveDate",
                                "midSepSequencingTActualSequenceDate",
                                "midSampleInfoTSamplingDate"]
        # self.tmp_file = '{}.tmp'.format(opts.output_file)

    def get_es_body(self, uuid):
        sql = "select t.es_sql from dvs_app_es_file_param_t t where t.union_id = '%s'"%(uuid)
        result = self.mysql_client.excute_sql_to_mysql(sql)
        for row in result:
            # result_json = json.dumps(row["es_sql"])
            # result_json["_source"].extend(self.default_key_list)
            # json.loads(new_tmp_content_str)
            return row["es_sql"]
        return None

    @retry(elasticsearch.exceptions.ConnectionError, tries=TIMES_TO_TRY)
    def create_connection(self):
        es = elasticsearch.Elasticsearch(self.opts.url, timeout=CONNECTION_TIMEOUT, http_auth=self.opts.auth,
                                         verify_certs=self.opts.verify_certs, ca_certs=self.opts.ca_certs,
                                         client_cert=self.opts.client_cert, client_key=self.opts.client_key,
                                         send_get_body_as='POST')
        es.cluster.health()
        self.es_conn = es

    @retry(elasticsearch.exceptions.ConnectionError, tries=TIMES_TO_TRY)
    def check_indexes(self):
        indexes = self.opts.index_prefixes
        if '_all' in indexes:
            indexes = ['_all']
        else:
            indexes = [index for index in indexes if self.es_conn.indices.exists(index)]
            if not indexes:
                print('Any of index(es) {} does not exist in {}.'.format(', '.join(self.opts.index_prefixes), self.opts.url))
                exit(1)
        self.opts.index_prefixes = indexes

    @retry(elasticsearch.exceptions.ConnectionError, tries=TIMES_TO_TRY)
    def search_query(self):
        @retry(elasticsearch.exceptions.ConnectionError, tries=TIMES_TO_TRY)
        def next_scroll(scroll_id):
            return self.es_conn.scroll(body={'scroll': self.scroll_time, 'scroll_id': scroll_id},request_timeout=30)

        search_args = dict(
            index=','.join(self.opts.index_prefixes),
            sort=','.join(self.opts.sort),
            scroll=self.scroll_time,
            size=self.opts.scroll_size,
            terminate_after=self.opts.max_results
        )

        if self.opts.doc_types:
            search_args['doc_type'] = self.opts.doc_types

        if self.opts.query.startswith('@'):
            query_file = self.opts.query[1:]
            if os.path.exists(query_file):
                with codecs.open(query_file, mode='r', encoding='utf-8') as f:
                    self.opts.query = f.read()
            else:
                print('No such file: {}.'.format(query_file))
                exit(1)
        if self.opts.raw_query:
            try:
                query = json.loads(self.opts.query)
            except ValueError as e:
                print('Invalid JSON syntax in query. {}'.format(e))
                exit(1)
            search_args['body'] = query
        else:
            query = self.opts.query if not self.opts.tags else '{} AND tags: ({})'.format(
                self.opts.query, ' AND '.join(self.opts.tags))
            search_args['q'] = query

        if '_all' not in self.opts.fields:
            # search_args['_source_excludes'] = ','.join(self.opts.fields)
            self.csv_headers.extend([unicode(field, "utf-8") for field in self.opts.fields if '*' not in field])

        if self.opts.debug_mode:
            print('Using these indices: {}.'.format(', '.join(self.opts.index_prefixes)))
            print('Query[{0[0]}]: {0[1]}.'.format(
                ('Query DSL', json.dumps(query, ensure_ascii=False).encode('utf-8')) if self.opts.raw_query else ('Lucene', query))
            )
            print('Output field(s): {}.'.format(', '.join(self.opts.fields)))
            print('Sorting by: {}.'.format(', '.join(self.opts.sort)))
        res = self.es_conn.search(**search_args)
        # res = self.es_conn.search(index=search_args["index"], 
        #                             body=search_args["body"],
        #                             sort=search_args["sort"], 
        #                             scroll=search_args["scroll"],
        #                             size=search_args["size"],
        #                             terminate_after=search_args["terminate_after"],
        #                             _source_include=search_args["_source_include"])
        self.num_results = res['hits']['total']

        print('Found {} results.'.format(self.num_results))
        if self.opts.debug_mode:
            print(json.dumps(res, ensure_ascii=False).encode('utf-8'))

        if self.num_results > 0:
            codecs.open(self.opts.output_file, mode='w', encoding='utf-8').close()
            # codecs.open(self.tmp_file, mode='w', encoding='utf-8').close()

            hit_list = []
            total_lines = 0

            widgets = ['Run query ',
                       progressbar.Bar(left='[', marker='#', right=']'),
                       progressbar.FormatLabel(' [%(value)i/%(max)i] ['),
                       progressbar.Percentage(),
                       progressbar.FormatLabel('] [%(elapsed)s] ['),
                       progressbar.ETA(), '] [',
                       progressbar.FileTransferSpeed(unit='docs'), ']'
                       ]
            #bar = progressbar.ProgressBar(widgets=widgets, maxval=self.num_results).start()

            while total_lines != self.num_results:
                if res['_scroll_id'] not in self.scroll_ids:
                    self.scroll_ids.append(res['_scroll_id'])

                if not res['hits']['hits']:
                    print('Scroll[{}] expired(multiple reads?). Saving loaded data.'.format(res['_scroll_id']))
                    break
                for hit in res['hits']['hits']:
                    total_lines += 1
                    #bar.update(total_lines)
                    hit_list.append(hit)
                    if len(hit_list) == FLUSH_BUFFER:
                        self.flush_to_file(hit_list)
                        hit_list = []
                    if self.opts.max_results:
                        if total_lines == self.opts.max_results:
                            self.flush_to_file(hit_list)
                            print('Hit max result limit: {} records'.format(self.opts.max_results))
                            return
                res = next_scroll(res['_scroll_id'])
            self.flush_to_file(hit_list)
            #bar.finish()

    def md5(self, string):
         h = hashlib.md5()
         h.update(string.encode(encoding='utf-8'))
         return str(h.hexdigest())

    def flush_to_file(self, hit_list):
        def to_keyvalue_pairs(source, ancestors=[], header_delimeter='.'):
            def is_list(arg):
                return type(arg) is list

            def is_dict(arg):
                return type(arg) is dict

            if is_dict(source):
                for key in source.keys():
                    to_keyvalue_pairs(source[key], ancestors + [key])

            elif is_list(source):
                if self.opts.kibana_nested:
                    [to_keyvalue_pairs(item, ancestors) for item in source]
                else:
                    [to_keyvalue_pairs(item, ancestors + [str(index)]) for index, item in enumerate(source)]
            else:
                header = header_delimeter.join(ancestors)
                if header not in self.csv_headers:
                    self.csv_headers.append(header)
                try:
                    out[header] = '{}{}{}'.format(out[header], self.opts.delimiter, source)
                except:
                    out[header] = source
        # self.new_write_to_csv(hit_list)
        tmp_content = []
        for hit in hit_list:
            out = {field: hit[field] for field in META_FIELDS} if self.opts.meta_fields else {}
            if '_source' in hit and len(hit['_source']) > 0:
                to_keyvalue_pairs(hit['_source'])
                cur_uuid = self.merge_str(out)
                if cur_uuid in self.uuid_list:
                # if self.uuid and self.uuid == cur_uuid:
                    continue
                tmp_content.append(out)
                self.uuid_list.append(cur_uuid)
                # self.uuid = cur_uuid
        tmp_content_str = json.dumps(tmp_content)
        new_tmp_content_str = tmp_content_str.strip().replace("\\n","").replace("\\r","").replace("\\t","")
        df = json_normalize(json.loads(new_tmp_content_str))
        # swtich date
        for col_field in df.columns.values.tolist():
            if col_field in self.date_swtich_list: df[col_field] = df[col_field].apply(self.modify_date)
        df.rename(columns=self.map_scv_header, inplace=True)
        # csv column mix wrong
        if self.csv_is_header:
            # export csv col sort
            if not df.empty:df.to_csv(self.opts.output_file, mode='a', header=False, index=False, encoding="utf_8", columns=self.opts.map_fields)
        else:
            self.csv_is_header = True
            if not df.empty:df.to_csv(self.opts.output_file, mode='a', header=True, index=False, encoding="utf_8_sig", columns=self.opts.map_fields)
    
    # switch date
    def modify_date(self, origin_date_str):
        if origin_date_str:
            local_time = dateutil.parser.parse(origin_date_str).astimezone(pytz.timezone('Asia/Shanghai'))
            local_date_str = datetime.datetime.strftime(local_time ,'%Y-%m-%d')
            return local_date_str
        else:
            return origin_date_str
    
    def merge_str(self, out):
        out_str = json.dumps(out)
        cur_uuid = self.md5(out_str)
        # vilad_key_list = list(set(self.default_key_list).intersection(set(self.opts.fields)))
        # vilad_key_list = list(set(self.opts.fields))
        # new_vilad_value_list = map(lambda x:"-" if not out[x] else out[x], vilad_key_list)
        # cur_uuid = str("#".join(new_vilad_value_list))
        return cur_uuid
    
    def write_to_csv(self):
        if self.num_results > 0:
            self.num_results = sum(1 for line in codecs.open(self.tmp_file, mode='r', encoding='utf-8'))
            if self.num_results > 0:
                output_file = codecs.open(self.opts.output_file, mode='a', encoding='utf_8_sig')
                print self.csv_headers
                if '_all' not in self.opts.map_fields:
                    csv_writer = csv.DictWriter(output_file, fieldnames=self.opts.map_fields)
                else:
                    csv_writer = csv.DictWriter(output_file, fieldnames=self.csv_headers)
                csv_writer.writeheader()
                timer = 0
                widgets = ['Write to csv ',
                           progressbar.Bar(left='[', marker='#', right=']'),
                           progressbar.FormatLabel(' [%(value)i/%(max)i] ['),
                           progressbar.Percentage(),
                           progressbar.FormatLabel('] [%(elapsed)s] ['),
                           progressbar.ETA(), '] [',
                           progressbar.FileTransferSpeed(unit='lines'), ']'
                           ]
                #bar = progressbar.ProgressBar(widgets=widgets, maxval=self.num_results).start()
                # utf_8_sig
                for line in codecs.open(self.tmp_file, mode='r', encoding='utf_8_sig'):
                    timer += 1
                    #bar.update(timer)
                    if '_all' not in self.opts.map_fields:
                        json_line = json.loads(line)
                        new_key = [self.map_scv_header[s] for s in json_line.keys()]
                        new_line = dict(zip(new_key, json_line.values()))
                        csv_writer.writerow(new_line)
                    else:
                        csv_writer.writerow(json.loads(line))
                output_file.close()
                #bar.finish()
            else:
                print('There is no docs with selected field(s): {}.'.format(','.join(self.opts.fields)))
            os.remove(self.tmp_file)

    def clean_scroll_ids(self):
        try:
            self.es_conn.clear_scroll(body=','.join(self.scroll_ids))
        except:
            pass
